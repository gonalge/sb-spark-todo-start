package ch.swissbytes.todo;

import ch.swissbytes.todo.views.ToDoListFreeMarkerEngine;
import spark.ModelAndView;
import spark.TemplateEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.SparkBase.stop;

public class ToDoListMain {
    public static void main(String[] args) {
        new ToDoListMain().startup();
    }

    public void startup() {
        EntityManager entityManager = new EntityManager();
        ToDoListFreeMarkerEngine templateEngine = new ToDoListFreeMarkerEngine();

        setupRoutes(entityManager, templateEngine);

        initialiseStartData(entityManager);
    }

    private void initialiseStartData(EntityManager entityManager) {
        entityManager.addNewItem("Item 1", "Description 1");
        entityManager.addNewItem("Item 2", "Description 2");
    }

    private void setupRoutes(EntityManager entityManager, ToDoListFreeMarkerEngine templateEngine) {
        setupItemsRoute(entityManager, templateEngine);
    }

    private void setupItemsRoute(EntityManager entityManager, TemplateEngine templateEngine) {
        get("/items", (request, response) -> {
            Map<String, Object> attributes = new HashMap<String, Object>();
            attributes.put("todoitems", entityManager.getItems());

            return new ModelAndView(attributes, "todolist.ftl");
        }, templateEngine);

        get("/items/addItem", (request, response) -> {
            Map<String, Object> attributes = new HashMap<String, Object>();
            //attributes.put("todoitems", entityManager.getItems());
            attributes.put("mensaje","");
            return new ModelAndView(attributes, "addItem.ftl");
        }, templateEngine);

        post("/items/addItem", (request, response) -> {
            Map<String, Object> attributes = new HashMap<String, Object>();
            String nombre = request.queryParams("nombre");
            String descripcion = request.queryParams("descripcion");
            entityManager.addNewItem(nombre,descripcion);
            attributes.put("mensaje","Se ha agregado el item correctamente");
            attributes.put("todoitems", entityManager.getItems());
            return new ModelAndView(attributes, "todolist.ftl");
        }, templateEngine);
    }

    public void shutdown() {
        stop();
    }
}
