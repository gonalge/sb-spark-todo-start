<h1>My ToDo List</h1>

<a href="/items/addItem" >[Add Item]</a>

<ul>
    <#list todoitems as item>
        <li>
            ${item.id} - ${item.title}
        </li>
    </#list>
</ul>