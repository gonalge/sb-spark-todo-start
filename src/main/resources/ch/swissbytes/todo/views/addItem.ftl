<h1> Agregar Nuevo Item </h1>

<#if mensaje?? >
    <b>${mensaje}</b>
</#if>

<form method="POST" action="/items/addItem">
    Nombre: <input type="text" name="titulo" /> <br/>
    Descripcion: <input type="text" name="descripcion" /> <br/>

    <br/>
    <input type="submit" value="Agregar" />
</form>

<a href="/items">[Lista de Items]</a>