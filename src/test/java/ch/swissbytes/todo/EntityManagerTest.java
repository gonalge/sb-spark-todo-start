package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class EntityManagerTest {
    private EntityManager entityManager;

    @Before
    public void setup() {
        entityManager = new EntityManager();
    }

    @Test
    public void getItemsReturnsEmptyListWhenNoItemsHaveBeenAdded() {
        assertThat(entityManager.getItems(), notNullValue());
        assertThat(entityManager.getItems().size(), is(0));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getItemsReturnsUnModifiableList() {
        entityManager.getItems().add(new ToDoItem(1, "title", "desc"));
    }

    @Test
    public void getItemsReturnsItemList() {
        ToDoItem item = entityManager.addNewItem("title", "description");
        assertThat(entityManager.getItems().size(), is(1));
        assertThat(entityManager.getItems(), contains(item));
    }

    @Test
    public void addNewItemAddsItemToList() {
        ToDoItem item = entityManager.addNewItem("title", "description");
        assertThat(entityManager.getItems().size(), is(1));
        assertThat(entityManager.getItems().get(0), is(item));
        assertThat(entityManager.getItems().get(0).getTitle(), is("title"));
        assertThat(entityManager.getItems().get(0).getDescription(), is("description"));
    }

    @Test
    public void addNewItemAssignsNewIdToEachItem() {
        ToDoItem item1 = entityManager.addNewItem("title1", "description1");
        ToDoItem item2 = entityManager.addNewItem("title2", "description2");
        assertThat(item1.getId(), not(item2.getId()));
        assertThat(item1.getId(), lessThan(item2.getId()));
    }
}
