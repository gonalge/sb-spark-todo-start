package ch.swissbytes.todo;

import org.junit.*;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class ItemsIntegrationTest {

    static final ToDoListMain app = new ToDoListMain();

    @BeforeClass
    public static void setup() throws InterruptedException {
        app.startup();
        Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines
    }

    @AfterClass
    public static void tearDown() {
        app.shutdown();
    }

    @Test
    public void willListTodoItems() {
        HelperMethods.UrlResponse response =
                HelperMethods.doMethod("GET", "/items", null);

        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length(), greaterThan(0));
        assertThat(200, is(response.status));
        assertThat(body, containsString("ToDo List"));
    }


    @Test
    public void willContainAddItem() {
        HelperMethods.UrlResponse response =
                HelperMethods.doMethod("GET", "/items", null);

        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length(), greaterThan(0));
        assertThat(200, is(response.status));
        assertThat(body,  containsString("[Add Item]"));
    }


}
