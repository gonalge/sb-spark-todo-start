package ch.swissbytes.todo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by admin on 9/8/2014.
 */
public class ItemsAddItemTest {

    static final ToDoListMain app = new ToDoListMain();

    @BeforeClass
    public static void setup() throws InterruptedException {
        app.startup();
        Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines
    }

    @AfterClass
    public static void tearDown() {
        app.shutdown();
    }

    @Test
    public void willShowAddItemForm() {
        HelperMethods.UrlResponse response =
                HelperMethods.doMethod("GET", "/items/addItem", null);

        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length(), greaterThan(0));
        assertThat(200, is(response.status));
        assertThat(body, containsString("<form"));
        assertThat(body, containsString("</form>"));

        assertThat(body, containsString("<input type=\"text\" name=\"titulo\""));
        assertThat(body, containsString("<input type=\"text\" name=\"descripcion\""));

        assertThat(body, containsString("<input type=\"submit\" value=\"Agregar\" />"));

    }


    @Test
    public void willShowAddItemPostForm() {
        HelperMethods.UrlResponse response =
                HelperMethods.doMethod("POST", "/items/addItem", "nombre=Geremias&descripcion=Hola%20Este%20mensaje");

        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length(), greaterThan(0));
        assertThat(200, is(response.status));

        assertThat(body, containsString("<b>Se ha agregado el item correctamente</b>"));


    }

}
